import React from 'react';
import './IntegerLabel.css';

class IntegerLabel extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div>
        {this.props.value}
      </div>
    );
  }

}

export default IntegerLabel;
