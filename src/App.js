import React from 'react';
import logo from './logo.svg';
import './App.css';

import IntegerInput from './IntegerInput/IntegerInput';
import IntegerLabel from './IntegerLabel/IntegerLabel';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      values: [...Array(Number(this.props.count)).keys()].map(i => 0)
    };
  }

  onChangeGenerator(num) {
    let x = function(value) {
      let values = this.state.values;
      values[num] = value;
      this.setState({
        count: Number(this.props.count),
        values: values
      });
    }
    x = x.bind(this);
    return x;
  }

  render() {
    return(
      <div>
        {[...Array(Number(this.props.count)).keys()].map(i => <IntegerInput value={this.state.values[i]} onChange={this.onChangeGenerator(i)} />)}
        <IntegerLabel value={this.state.values.reduce((a, b) => Number(a) + Number(b))}/>
      </div>
    );
  }

}

export default App;
