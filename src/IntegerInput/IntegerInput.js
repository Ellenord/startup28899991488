import React from 'react';
import './IntegerInput.css';

class IntegerInput extends React.Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.props.onChange(e.target.value.replace(/\D/g, ''));
  }

  render() {
    return(
      <div>
        <input onChange={this.onChange} value={this.props.value} type="text" />
      </div>
    );
  }

}

export default IntegerInput;
